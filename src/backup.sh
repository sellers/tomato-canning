#!/bin/env sh
# Paste this script into any scheduled job - weekly at least
# requires a USB mount or some other path
#
# Tested with Netgear WNDR3400 series
# Works with ASUS RT routers and likely most others
# 
# Set frequency to your liking, hourly will keep two sets of files
# hazards: if you change frequency, filenames may conflict

frequency='weekly'        # values are  weekly, daily, monthly


BASEDIR=/tmp/mnt/sda2               # change this based on USB mount
DEVICEDIR=$(nvram get t_fix1)       # avoids collisions if you switch router hardware
BACKUPFILE=backup.cfg
INIT=$(ls "$BASEDIR/${DEVICEDIR}" || mkdir -p "$BASEDIR/${DEVICEDIR}")

function dobackup() {
    set -x
    result=$(cp "${BASEDIR}/${DEVICEDIR}/${BACKUPFILE}" "${BASEDIR}/${DEVICEDIR}/$BACKUPFILE.$1")
    printf "Executed command, return $result : $?.\n"
}

function doexport() {
    nvram export --dump | tee "$BASEDIR/${DEVICEDIR}/${BACKUPFILE}"        # tee should dump to syslog
}

if [[ $frequency == 'weekly' ]]
then
    NOW=$(date +%W)
    doexport
    dobackup "W$NOW"

elif [[ $frequency == 'daily' ]]
then
    NOW=$(date +%d)
    doexport
    dobackup "D$NOW"
elif [[ $frequency == 'monthly' ]]
then
    NOW=$(date +%m)
    doexport
    dobackup "M$NOW"
elif [[ $frequency == 'hourly' ]]
then
    doexport
    NOW=$(date +%H)
    dobackup "H$NOW"
    DAY=$(date +%d)
    dobackup "D$DAY"
else
    doexport
    NOW=$(date +%d)                 # if you mess up at least try do a daily
    dobackup "$NOW"
fi

exit 0
