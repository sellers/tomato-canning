# tomato-canning
Firmware Config nvram saving utility for tomato flavored firmwares

This is the first version that I am posting here to save for my own records

Usage:
place this on your tomato router in the scripts section or on firmware partition
and then setup a schedule for it to run. Uses a 365/24 rotation method where it
is designed to save a file every hour with the hour in the filename, and each
day, save a file with the day of the year in the filename. This means you get
auto-rotation of the files after 365 days and every four years or so you will
have a saved file that lasts between leap years

